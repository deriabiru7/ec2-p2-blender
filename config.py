import bpy

# Use GPU to render.
bpy.context.user_preferences.addons['cycles'].preferences.compute_device_type = 'CUDA'
bpy.context.user_preferences.addons['cycles'].preferences.devices[0].use = True

bpy.context.scene.cycles.device = 'GPU'

# The resolution of smoke/fire.
bpy.data.objects["Smoke Domain"].modifiers["Smoke"].domain_settings.resolution_max = 128

# Enhance the resolution of smoke by this factor using noise.
bpy.data.objects["Smoke Domain"].modifiers["Smoke"].domain_settings.amplify = 10

# Noise strength.
bpy.data.objects["Smoke Domain"].modifiers["Smoke"].domain_settings.strength = 10

# Final output in MPEG4 format.
bpy.data.scenes["Scene"].render.filepath = "/home/ec2-user/"
bpy.data.scenes["Scene"].render.image_settings.file_format = 'FFMPEG'
bpy.data.scenes["Scene"].render.ffmpeg.format = "MPEG4"
bpy.data.scenes["Scene"].render.ffmpeg.codec = "H264"
bpy.data.scenes["Scene"].render.ffmpeg.constant_rate_factor = "LOSSLESS"
bpy.data.scenes["Scene"].render.ffmpeg.ffmpeg_preset = "BEST"

# Render specifically these frames,
#bpy.data.scenes["Scene"].frame_start = 45
#bpy.data.scenes["Scene"].frame_end = 125

bpy.ops.render.render()
