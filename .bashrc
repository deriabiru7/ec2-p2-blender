# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export EDITOR=/usr/bin/vim

PS1='\[\033[01;31m\]\u@\h\[\033[01;34m\] \W \$\[\033[00m\] '
alias ls="ls --color=auto"
alias ll="ls -lSrth"
alias rm="rm -vi"
alias mv="mv -i"
alias cp="cp -i"
alias grep="grep --color=auto"
